/*
 * STM32_Driver.c
 *
 *  Created on: 6 juin 2023
 *      Author: turmat
 */

#include <FDCAN_Datas.h>
#include "main.h"
#include "ErrorFile.h"
#include "FDCAN_lib.h"

#define CAN_GENERATE_ID(identifier,function,CANidentType,CANframeType)    ( (unsigned short)((identifier) << 3 ) | (unsigned short)((((function) << 3) | (CANidentType) | (CANframeType))) )




<<<<<<< Updated upstream
void CreateFDCAN_Filterlist (struct FDCAN *FDCAN)
=======
void CreateFDCAN_Filterlist (struct CANLib *FDCAN)
>>>>>>> Stashed changes
{
    CANLib_AddFilter(
            FDCAN,
            FDCAN_FILTER_DUAL,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_TEMPERATURES), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(My_Id, (COMMAND_FUNCTION_TYPE + SEND_BOOTLOADER_PLAY), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
);
        //--- Configuration ---


    CANLib_AddFilter(
            FDCAN,
            FDCAN_FILTER_MASK,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(Battery_Id, 0, FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(CAN_MASK_FAMILY, (0), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
            );
        //--- Configuration ---


    CANLib_AddFilter(
            FDCAN,
            FDCAN_FILTER_DUAL,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_TEMPERATURES), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_BATTERY_SETTINGS), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
            );
        //--- Configuration ---


    CANLib_AddFilter(
            FDCAN,
            FDCAN_FILTER_DUAL,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_INFO_ETAT), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_INFO_UTILISATION), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
            );
        //--- Configuration ---


    CANLib_AddFilter(
            FDCAN,
            FDCAN_FILTER_DUAL,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_FUNCTIONNAL_COUNTERS), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_SPEED_SETTINGS), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
            );
        //--- Configuration ---


    CANLib_AddFilter(
            //--- Configuration ---
            FDCAN,
            FDCAN_FILTER_DUAL,
            FDCAN_FILTER_TO_RXFIFO0,
            CAN_GENERATE_ID(PCDiag_Id, (COMMAND_FUNCTION_TYPE + BC_MODE), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
            CAN_GENERATE_ID(Motor_Id, (STATIC_FUNCTION_TYPE + MC_IDENTIFICATION), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
            );


    CANLib_AddFilter(
                FDCAN,
                FDCAN_FILTER_DUAL,
                FDCAN_FILTER_TO_RXFIFO0,
                CAN_GENERATE_ID(PCDiag_Id, (STATIC_FUNCTION_TYPE + SEND_IDENTIFICATION), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
                CAN_GENERATE_ID(Motor_Id, (COMMAND_FUNCTION_TYPE + BOOTLOADER_STATE), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
           );


    CANLib_AddFilter(
               FDCAN,
               FDCAN_FILTER_MASK,
               FDCAN_FILTER_TO_RXFIFO0,
               CAN_GENERATE_ID(0xFFF8, 0x0000, FDCAN_EXTENDED_ID, FDCAN_REMOTE_FRAME),
               CAN_GENERATE_ID(My_Id, (0x0000), FDCAN_EXTENDED_ID, FDCAN_REMOTE_FRAME)
            );
    CANLib_AddFilter(
                FDCAN,
                FDCAN_FILTER_DUAL,
                FDCAN_FILTER_TO_RXFIFO0,
                CAN_GENERATE_ID(Motor_Id, (DYNAMIC_FUNCTION_TYPE + MC_INFO_CONSO), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME),
                CAN_GENERATE_ID(My_Id, (COMMAND_FUNCTION_TYPE + CMD_MODE_FABTEST), FDCAN_EXTENDED_ID, FDCAN_DATA_FRAME)
                );
}

void can_callback(FDCAN_HandleTypeDef *fdcan, const FDCAN_RxHeaderTypeDef *header, const uint8_t *data){

}

eERRORRESULT CANStart (FDCAN_HandleTypeDef *hfdcan){

     struct CANLib FDCAN;

    FDCAN_TxHeaderTypeDef txHeader;
    // initialize the tx header
    txHeader.Identifier = 0x321;
    txHeader.IdType = FDCAN_STANDARD_ID;
    txHeader.TxFrameType = FDCAN_DATA_FRAME;
    txHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
    txHeader.BitRateSwitch = FDCAN_BRS_OFF;
    txHeader.FDFormat = FDCAN_CLASSIC_CAN;
    txHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
    txHeader.MessageMarker = 0;

    if (CANLib_Init(&FDCAN, &hfdcan2, &txHeader) != 0) Error_Handler();
<<<<<<< Updated upstream

=======
    CANLib_Start(&FDCAN);
>>>>>>> Stashed changes
    if (CANLib_SetFilterMode(&FDCAN, CANLIB_NONMATCH_REJECT, CANLIB_NONMATCH_REJECT) != 0)
          Error_Handler();

    CreateFDCAN_Filterlist(&FDCAN);
    if (CANLib_SetReceiveCallback(0, can_callback));

    if (CANLib_Start(&FDCAN) != 0) Error_Handler();

<<<<<<< Updated upstream

=======
return ERR_NONE;
>>>>>>> Stashed changes
}

eERRORRESULT CANStop (FDCAN_HandleTypeDef *hfdcan){

    uint8_t error;
    error = HAL_FDCAN_Stop(hfdcan);

    if (error != HAL_OK){
<<<<<<< Updated upstream
        HAL_to_Error(error);
=======
        HALtoERRORRESULT(error);
>>>>>>> Stashed changes
        return error;
    }
        return ERR_NONE;
}





