/*******************************************************************************
SUJET           :   Fichier IS31FL3736.c
AUTEUR          :   M.TURELIER
VERIFICATEUR    :
Date de Creation:   09.03.2023
MicroControleur :   STM32

ENVIRONNEMENT   :   ECLIPSE
VERSION         :   PHOTON 4.8.0

DESCRIPTION     :   IHM Multi-Outil LED Driver

(c)PELLENC SA FRANCE

********************************************************************************/

#include <stdbool.h>
#include "main.h"
#include "IS31FL3736_Driver.h"
#include "stm32g0xx_hal_i2c.h"
#include "ErrorFile.h"
#include "7segDisplay_driver.h"


uint8_t Global_Current_Page;
uint8_t Leds_Picto_Mask [2];

eERRORRESULT setIS31FL3736_LEDs (LEDSwitchList eSW, uint16_t cs)
{
    uint32_t TimeOutI2C = 500000u;
<<<<<<< Updated upstream
    uint16_t i =0;
    uint8_t JM_Data[3];
=======
<<<<<<< Updated upstream
=======
    uint8_t JM_Data[3];
>>>>>>> Stashed changes
>>>>>>> Stashed changes
    uint8_t JM_Byte[3] = {(uint8_t)eSW, (uint8_t)cs, (uint8_t)(cs >> 8)};
    switchPage(LED_Control_PAGE);
<<<<<<< Updated upstream
=======
<<<<<<< Updated upstream
=======
>>>>>>> Stashed changes
    switch (eSW){
        case DIG1_RED :
                    JM_Data[0]=DIG1_WHITE;
                      break;
        case DIG2_RED :
                        JM_Data[0]=DIG2_WHITE;
            break;
        case DIG1_WHITE :
                         JM_Data[0]=DIG1_RED;
                         break;
        case DIG2_WHITE :
                         JM_Data[0]=DIG2_RED;
                      break;
<<<<<<< Updated upstream
=======
        default :
            break;
>>>>>>> Stashed changes
        //case PICTO_
    }

    JM_Data[1]=0x00;
    JM_Data[2]=0x00;
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);

<<<<<<< Updated upstream
=======
>>>>>>> Stashed changes
>>>>>>> Stashed changes
    if (HAL_I2C_IsDeviceReady(&hi2c2, Addr_GND_GND, 5, TimeOutI2C)==HAL_OK){
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Byte[0], sizeof (JM_Byte), TimeOutI2C);
        return ERR_NONE;
    }

    return ERR__I2C_BUSY;
}



eERRORRESULT switchPage (enum pageSelector ps)
{
    uint32_t TimeOutI2C = 500000u;
    if (Global_Current_Page != ps){
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, IS31_UNLOCK, TimeOutI2C);
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, (uint8_t [2]){0xFD, ps}, 2, TimeOutI2C);
        Global_Current_Page = ps;
    }


    return ERR_NONE;
}


eERRORRESULT setIS31FL3736_ChangeValidation (void)
{
    uint8_t JM_Data[2];
    uint32_t TimeOutI2C = 500000u;
    switchPage(FUNC_Control_PAGE);
    JM_Data[0]=0x01;
    JM_Data[1]=0x55;
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);//normal operation
}


eERRORRESULT setPWM (uint8_t PWMValue){
    eERRORRESULT Error;
    uint32_t TimeOutI2C = 500000u;
    uint16_t i=0;
    uint8_t JM_Data[2];
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, IS31_UNLOCK, TimeOutI2C);//unlock FDh
    Error = switchPage(PWM_Control_PAGE);
    if (Error != ERR_NONE){
        return Error;
    }
    for(i=0;i<192;i=i+2){
        JM_Data[0]=i;
        JM_Data[1]=PWMValue;
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);
        return ERR_NONE;
    }
    return ERR__I2C_BUSY;
}

eERRORRESULT IS31FL3736_Init(void)
{
       uint8_t i;
       uint32_t TimeOutI2C = 500000u;
       uint8_t JM_Data[2];
       uint16_t Error;
       HAL_GPIO_WritePin(SDB_GPIO_Port, SDB_Pin, GPIO_PIN_SET);
         switchPage(LED_Control_PAGE);
         for (i=0; i<24; i++){
             JM_Data[0]=i;
             JM_Data[1]=0x00;
             Error=HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);//open all led
         }
         //setPWM(0x7F);
         switchPage(FUNC_Control_PAGE);
         JM_Data[0]=0x00;
         JM_Data[1]=0x01;
         HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);
         IS31FL3736_Global_Current(0x7F);

         if (Error == HAL_OK) return ERR_NONE;
         else return Error;
}


eERRORRESULT IS31FL3736_Test_All_On_Off(void)
{
    uint32_t TimeOutI2C = 500000u;
    uint8_t JM_Data[2];


    setIS31FL3736_LEDs(DIG1_RED, SEG7_NBR_0);
    setIS31FL3736_LEDs(DIG2_RED, SEG7_NBR_8);
    setIS31FL3736_LEDs(PICTO_BTRY_25);
    setPWM(0x01);
    setIS31FL3736_ChangeValidation();
    setIS31FL3736_LEDs(DIG1_RED, SEG7_NBR_8);
    setIS31FL3736_LEDs(DIG2_WHITE, SEG7_NBR_7);
    setIS31FL3736_LEDs(PICTO_BTRY_50);
    setPWM(0xFF);
   //00h set 0X01 PWM=8.4K///00h set 0X11 PWM=26.7K//00h set 0X09 PWM=4.2K//00h set 0X19 PWM=2.1K//00h set 0X21 PWM=1.05K
    JM_Data[0]=0x00;
    JM_Data[1]=0x01;
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);
    IS31FL3736_Global_Current(0x55);

    return ERR_NONE;
}


eERRORRESULT IS31FL3736_Select_Sw_Pull(uint8_t dat)
{
    uint32_t TimeOutI2C = 500000u;
    uint8_t JM_Data[2];
        JM_Data[0] = 0xFE;
        JM_Data[1] = 0xC5;
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);//unlock FDh
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, IS31_PAGE3, TimeOutI2C);
        JM_Data[0] = 0x0F;
        JM_Data[1] = dat;
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);
}


eERRORRESULT IS31FL3736_Select_Cs_Pull(uint8_t dat)
{
    uint32_t TimeOutI2C = 500000u;
    uint8_t JM_Data[2];
        JM_Data[0] = 0xFE;
        JM_Data[1] = 0xC5;
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);//unlock FDh
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, IS31_PAGE3, TimeOutI2C);
        JM_Data[0] = 0x10;
        JM_Data[1] = dat;
        HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0], sizeof(JM_Data), TimeOutI2C);
}


eERRORRESULT IS31L3736_Reset(void)
{
    uint8_t JM_Data;
    uint32_t TimeOutI2C = 500000u;
    switchPage(FUNC_Control_PAGE);
    HAL_I2C_Master_Receive(&hi2c2, Addr_GND_GND_Read, &JM_Data, 1, TimeOutI2C);

   return ERR_NONE;
}

eERRORRESULT IS31FL3736_Global_Current(uint8_t dat)
{
    uint32_t TimeOutI2C = 500000u;
    uint8_t JM_Data[2];
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, IS31_UNLOCK, TimeOutI2C);//unlock FDh
    switchPage(FUNC_Control_PAGE);//write page 3
    JM_Data[0]= 0x01;
    JM_Data[1]=dat;
    HAL_I2C_Master_Transmit(&hi2c2, Addr_GND_GND, &JM_Data[0],2, TimeOutI2C);
     return ERR_NONE;
}
