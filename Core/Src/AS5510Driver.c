/*
 * AS5510Driver.c
 *
 *  Created on: 20 avr. 2023
 *      Author: turmat
 */

#include "AS5510Driver.h"
#include <stdbool.h>
#include "main.h"


#define UNIT_ERR_CONTEXT  ERRCONTEXT__AS5510

AS5510Driver AS5510;



void InitAS5510 (void)
{

    volatile unsigned long TimeOutI2C = 500000u;
    _Bool init_ok = false;

#ifdef EXELION_POIGNEE
    SensibiliteCapteur =    SENS_25mG;
    #else
    AS5510.SensibiliteCapteur =    SENS_12mG;
#endif
    AS5510.PreviousSensibiliteCapteur = SENS_50mG;

#ifdef INVERT_MAGNET
    Polarity = INVERT_POLARITY;
    OldPolarity = DIRECT_POLARITY;
#else
    AS5510.Polarity = DIRECT_POLARITY;
    AS5510.PreviousPolarity = INVERT_POLARITY;
#endif

    if( AS5510.SensibiliteCapteur != AS5510.PreviousSensibiliteCapteur )
    {
        while( InitSensitivityAS5510(AS5510.SensibiliteCapteur) < 0 )
        {
            if(TimeOutI2C-- == 0u) break;
        }
        AS5510.PreviousSensibiliteCapteur = AS5510.SensibiliteCapteur;
    }

    TimeOutI2C = 500000u;
    if( AS5510.Polarity != AS5510.PreviousPolarity )
    {
        while( InitPolarityAS5510( AS5510.Polarity) < 0 )
        {
            if(TimeOutI2C-- == 0u) break;
        }
        AS5510.PreviousPolarity = AS5510.Polarity;
    }

    AS5510.CapteurAddress = ADDR_AS5510;
    init_ok = true;
}

eERRORRESULT InitPolarityAS5510(unsigned char Polarity)
{
    volatile unsigned long TimeOutI2C = 500000u;
    HAL_StatusTypeDef Error;
    // Check if I2C is idle or busy
    while (HAL_I2C_IsDeviceReady(&hi2c2, AS5510.CapteurAddress, 2 , TimeOutI2C ))
    {
        if(TimeOutI2C-- == 0u) return ERR__TIMEOUT;
    }
    //------------------------------------------------------------------------
    // Set the salve address to be transmitted after start condition
    Error = HAL_I2C_Master_Transmit(&hi2c2, AS5510.CapteurAddress, (uint8_t*)&Polarity, 1, TimeOutI2C);
    //------------------------------------------------------------------------
    // Step4 - Send STOP condition
    if (Error != HAL_OK)
    {
        return ERR_GENERATE(HALtoERRORRESULT(Error));;
    }
    return ERR_NONE;
}

eERRORRESULT InitSensitivityAS5510(unsigned char Sensitivity)
{
    volatile unsigned long TimeOutI2C = 500000u;
    HAL_StatusTypeDef Error;
    // Check if I2C is idle or busy
    while (HAL_I2C_IsDeviceReady(&hi2c2, AS5510.CapteurAddress, 2 , TimeOutI2C ))
    {
        if(TimeOutI2C-- == 0u) return ERR__TIMEOUT;
    }
    //------------------------------------------------------------------------
    // Set the salve address to be transmitted after start condition
    Error = HAL_I2C_Master_Transmit(&hi2c2, AS5510.CapteurAddress, (uint8_t*)&Sensitivity, 1, TimeOutI2C);
    //------------------------------------------------------------------------
    // Step4 - Send STOP condition
    if (Error != HAL_OK)
    {
        return ERR_GENERATE(HALtoERRORRESULT(Error));
    }
    return ERR_NONE;
}




/*******************************************************************************
 * Function:            eERRORRESULT ReadAS551O(unsigned char Address, unsigned char *value )
 * PreCondition:        None
 * Input:               Address: address of register to read the data
 *                      *value: pointer that containt the data
 * Output:              1 if transmission success / 0 if failed
 * Side Effects:        None
 * Stack Requirements:  1 level deep
 * Overview:            This function send initialization settings to the sensor
*******************************************************************************/
eERRORRESULT ReadAS551O(unsigned char DataAddress, uint16_t *AsData )
{
    volatile unsigned long TimeOutI2C = 500000u;

    // Check if I2C is idle or busy

    HAL_StatusTypeDef Error;
    // Check if I2C is idle or busy
    while (HAL_I2C_IsDeviceReady(&hi2c2, AS5510.CapteurAddress, 2 , TimeOutI2C ))
    {
        if(TimeOutI2C-- == 0u) return ERR_GENERATE(ERR__TIMEOUT);
    }
    //------------------------------------------------------------------------
    // Set the salve address to be transmitted after start condition
    Error = HAL_I2C_Master_Transmit(&hi2c2, AS5510.CapteurAddress, (uint8_t*)&DataAddress, 1, TimeOutI2C);

    TimeOutI2C = 500000u;
    if (Error != HAL_OK)
    {
        return ERR_GENERATE(HALtoERRORRESULT(Error));
    }
    //------------------------------------------------------------------------



    // Step4 - Wait end of transmission
    TimeOutI2C = 500000u;

    HAL_I2C_EnableListen_IT(&hi2c2);
    HAL_I2C_Master_Receive(&hi2c2, AS5510.CapteurAddress,(uint8_t*) &AsData, 2, TimeOutI2C);


        if(TimeOutI2C-- == 0u) return ERR_GENERATE(ERR__TIMEOUT);



    return ERR_NONE;
}

/*******************************************************************************
 * Function:            char AcquisitionAS5510( unsigned int *value)
 * PreCondition:        None
 * Input:               *value: pointer that containt the data
 * Output:              1 if transmission success / 0 if failed
 * Side Effects:        None
 * Stack Requirements:  1 level deep
 * Overview:            This function send initialization settings to the sensor
*******************************************************************************/
inline eERRORRESULT AcquisitionAS5510(uint16_t *value)
{
    return ReadAS551O(ADDR_LSB_AS5510, value);
}
/***************************** END OF FILE ********************************/
