/*
 * IS31FL3736_Driver.h
 *
 *  Created on: 20 avr. 2023
 *      Author: turmat
 */

#ifndef INC_IS31FL3736_DRIVER_H_
#define INC_IS31FL3736_DRIVER_H_

/*
 * IS31FL3736.h
 *
 *  Created on: 3 avr. 2023
 *      Author: turmat
 */

#ifndef INC_IS31FL3736_H_
#define INC_IS31FL3736_H_

#include "main.h"
#include "ErrorFile.h"


eERRORRESULT IS31FL3736_Select_Sw_Pull(uint8_t dat);
eERRORRESULT IS31FL3736_Select_Cs_Pull(uint8_t dat);
eERRORRESULT IS31FL3736_Test_All_On_Off(void);
eERRORRESULT IS31FL3736_Brightness_Level(uint8_t dat);
eERRORRESULT IS31FL3736_Init(void);
eERRORRESULT IS31FL3736_Global_Current(uint8_t dat);
eERRORRESULT IS31FL3736_PWM_ALL_ON(void);
eERRORRESULT IS31FL3736_PWM_ALL_OFF(void);
eERRORRESULT IS31FL3736_Reset_Pwm_Value(void);
eERRORRESULT IS31FL3736_Reset_Resigester(void);
eERRORRESULT IS31FL3736_ABMS_Control(uint8_t dat);
eERRORRESULT IS31FL3736_ABMS_Reset(void);
eERRORRESULT IS31FL3736_ABMS_Breath_Time(void);
eERRORRESULT Play_IS31FL3736_Demo_mode(void);
eERRORRESULT handle_IS31FL3736(void);
eERRORRESULT IS31L3736_Reset(void);
eERRORRESULT setIS31FL3736_ChangeValidation (void);


#define Addr_GND_GND  0xA0
#define Addr_GND_GND_Read  0xA1
#define Addr_GND_SCL  0xA2
#define Addr_GND_SDA  0xA4
#define Addr_GND_VCC  0xA6
#define Addr_SCL_GND  0xA8
#define Addr_SCL_SCL  0xAA
#define Addr_SCL_SDA  0xAC
#define Addr_SCL_VCC  0xAE
#define Addr_SDA_GND  0xB0
#define Addr_SDA_SCL  0xB2
#define Addr_SDA_SDA  0xB4
#define Addr_SDA_VCC  0xB6
#define Addr_VCC_GND  0xB8
#define Addr_VCC_SCL  0xBA
#define Addr_VCC_SDA  0xBC
#define Addr_VCC_VCC  0xAE
#define Addr_GND      0xE8//3731

enum pageSelector {
    LED_Control_PAGE,
    PWM_Control_PAGE,
    AMS_Control_PAGE,
    FUNC_Control_PAGE
};


#define IS31_UNLOCK (uint8_t [2]) {0xFE, 0xC5}, 2 //Demmerde Toi !!!!
#define IS31_PAGE0 (uint8_t [2]) {0xFD, LED_Control_PAGE}, 2
#define IS31_PAGE1 (uint8_t [2]) {0xFD, PWM_Control_PAGE}, 2
#define IS31_PAGE2 (uint8_t [2]) {0xFD, AMS_Control_PAGE}, 2
#define IS31_PAGE3 (uint8_t [2]) {0xFD, FUNC_Control_PAGE}, 2



#define CS1 (1 << 0  )
#define CS2 (1 << 2  )
#define CS3 (1 << 4  )
#define CS4 (1 << 6  )
#define CS5 (1 << 8  )
#define CS6 (1 << 10 )
#define CS7 (1 << 12 )
#define CS8 (1 << 14 )


//! This macro is used to check the size of an object. If not, it will raise a "divide by 0" error at compile time
#define CONTROL_ITEM_SIZE(item, size)  enum { item##_size_must_be_##size##_bytes = 1 / (int)(!!(sizeof(item) == size)) }

#define __IS31FL3736_PACKED__           __attribute__((packed))
#define IS31FL3736_PACKITEM
#define IS31FL3736_UNPACKITEM
#define IS31FL3736_PACKENUM(name,type)  typedef enum __IS31FL3736_PACKED__
#define IS31FL3736_UNPACKENUM(name)     name

IS31FL3736_PACKENUM(LEDSwitchList, uint8_t)
{
    SW1 = 0x00,
    SW2 = 0x02,
    SW3 = 0x04,
    SW4 = 0x06,
    SW5 = 0x08,
    SW6 = 0x0A,
    SW7 = 0x0C,
    SW8 = 0x0E,
    SW9 = 0x10,
    SW10 = 0x12,
    SW11 = 0x14,
    SW12 = 0x16
} IS31FL3736_UNPACKENUM(LEDSwitchList);
CONTROL_ITEM_SIZE(LEDSwitchList, 1);



typedef union WriteSeg7
 {
        uint8_t Byte[3];
        struct {
                LEDSwitchList SWx;
                uint16_t CSx;
        };
}WriteSeg7;

eERRORRESULT setIS31FL3736_LEDs (LEDSwitchList eSW, uint16_t cs);
eERRORRESULT switchPage (enum pageSelector ps);
eERRORRESULT setPWM (uint8_t PWMValue);



#endif /* INC_IS31FL3736_H_ */


#endif /* INC_IS31FL3736_DRIVER_H_ */
