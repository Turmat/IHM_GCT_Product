/*
 * FDCAN_Driver.h
 *
 *  Created on: 6 juin 2023
 *      Author: turmat
 */

#ifndef INC_FDCAN_DATAS_H_
#define INC_FDCAN_DATAS_H_

/*--- TOOL SETTINGS --------------------------------------------------------*/

#define BROADCAST_ID                        0
#define HMI_ID                              1
#define BATTERY_EXD_ID                      2
#define CONT_MOT_EXD_ID                     3
#define SENSORS_ID                          4
#define CHARGER_ID                          5
#define GPS_MODULE_ID                       6
#define DIAG_PC_ID                          7


#define FAMILY                              4
#define SUB_FAMILY                          1
#define CARD_NUMBER                         1

#define CAN_MASK_FAMILY                     0x1F000000

#define My_Id                               (uint32_t)((FAMILY << 24)+(SUB_FAMILY<<21)+(CARD_NUMBER<<16))
#define BroadCast_Id                        (uint32_t)((BROADCAST_ID<<24)+(0<<21)+(0<<16))
#define Motor_Id                            (uint32_t)((CONT_MOT_EXD_ID<<24)+(SUB_FAMILY<<21)+(CARD_NUMBER<<16))
#define Battery_Id                          (uint32_t)((BATTERY_EXD_ID<<24)+(SUB_FAMILY<<21)+(CARD_NUMBER<<16))
#define PCDiag_Id                           (uint32_t)((DIAG_PC_ID<<24)+(SUB_FAMILY<<21)+(CARD_NUMBER<<16))


/*******************************************************************************
 *                  MOTOR-CONTROL BOARD FUNCTIONS
********************************************************************************/
/*--- STATIC SETTING ----------------------------------------------------------*/
#define MC_IDENTIFICATION                   0x0020
#define MC_MOTOR_SETTINGS                   0x0021
#define MC_SPEED_COMPENSATOR                0x0022
#define MC_CURRENT_COMPENSATOR              0x0023
#define MC_FUNCTIONNAL_SETTINGS             0x0024
#define MC_SERIAL_NUMBER                    0x0025
#define MC_TOOL_DESCRIPTION_1               0x0026
#define MC_TOOL_DESCRIPTION_2               0x0027


/*--- DYNAMIC SETTINGS --------------------------------------------------------*/
#define MC_TRIGGER_SETTINGS                 0x0001
#define MC_SPEED_SETTINGS                   0x0002
#define MC_BATTERY_SETTINGS                 0x0003
#define MC_IQ_SETTINGS                      0x0004
#define MC_ID_SETTINGS                      0x0005
#define MC_VOLTAGE_SETTINGS                 0x0006
#define MC_TEMPERATURES                     0x0007
#define MC_ACCELERATIONS_SETTINGS           0x0008
#define MC_FUNCTIONNAL_COUNTERS             0x0009
//#define MC_CONSOMMATION                       0x000A
#define MC_INFO_UTILISATION                 0x000A
#define MC_INFO_ETAT                        0x000B
#define MC_INFO_CONSO                       0x000C

/*--- SPECIFIC REQUEST SETTINGS -----------------------------------------------*/
#define MC_START_DYNAMIQUES_PARAM           0x00F0
#define MC_STOP_DYNAMIQUES_PARAM            0x00F1
#define MC_REQUETE_STATIC                   0x00F2
#define MC_START_POSI_MEASUREMENT           0x00F3



/*******************************************************************************
 *                  PC-DIAG BOARD FUNCTIONS
********************************************************************************/
/*--- STATIC SETTING ----------------------------------------------------------*/
#define PC_IDENTIFICATION                   0x0020
/*--- DYNAMIC SETTINGS --------------------------------------------------------*/
/*--- SPECIFIC REQUEST SETTINGS -----------------------------------------------*/
/*--- COMMAND SETTINGS --------------------------------------------------------*/


#define BROADCAST_ID                        0
#define HMI_ID                              1
#define BATTERY_EXD_ID                      2
#define CONT_MOT_EXD_ID                     3
#define SENSORS_ID                          4
#define CHARGER_ID                          5
#define GPS_MODULE_ID                       6
#define DIAG_PC_ID                          7


/*--- FUNCTIONS TYPE ----------------------------------------------------------*/
#define STATIC_FUNCTION_TYPE                0x0100
#define DYNAMIC_FUNCTION_TYPE               0x0200
#define REQUEST_FUNCTION_TYPE               0x0300
#define COMMAND_FUNCTION_TYPE               0x0400
/*--- REQUEST SETTINGS --------------------------------------------------------*/
#define START_SEND_DYNAMICS_SETTINGS        0x00F0
#define STOP_SEND_DYNAMICS_SETTINGS         0x00F1
#define START_SEND_STATICS_SETTINGS         0x00F2
#define REQUEST_POSITION                    0x00F3
#define START_BANC_DE_TEST                  0x00F4
#define STOP_BANC_DE_TEST                   0x00F5

/*--- COMMAND SETTINGS --------------------------------------------------------*/
#define SEND_IDENTIFICATION                 0x20                                // Envoi identification de la machine
#define SEND_BOOTLOADER_RESET               0x2E                                // Envoi demande reset carte
#define SEND_BOOTLOADER_PLAY                0x2E                                // Envoi start application
#define SEND_BOOTLOADER_CRC                 0x2E                                // Envoi demande controle CRC
#define SEND_BOOTLOADER_ADDRESS_BLOC        0x2E                                // Envoi taille et adresse depart du bloc de donnees
#define BOOTLOADER_STATE                    0x2D                                // Envoi Etat Bootloader CAN
#define BOOTLOADER_COMMAND                  0x2E                                // Envoi commande
#define BOOTLOADER_DATA                     0x2F                                // Envoi datas du bloc

#define STATE_MSG_ID                        (uint32_t)( My_Id + COMMAND_FUNCTION_TYPE + BOOTLOADER_STATE )  //0xMMMM042D
#define CTRL_MSG_ID                         (uint32_t)( My_Id + COMMAND_FUNCTION_TYPE + BOOTLOADER_COMMAND )//0xMMMM042E
#define DATA_MSG_ID                         (uint32_t)( My_Id + COMMAND_FUNCTION_TYPE + BOOTLOADER_DATA )   //0xMMMM042F

#define DMD_RESET                           0x5A                                // Demande reset �P
#define DMD_RUN_BOOTLOADER                  0xA5                                // Demande Run Bootloader
#define DMD_RUN_API                         0xA6                                // Demande Run API
#define DMD_CTRL_CRC                        0x55                                // Demande controle CRC

/*******************************************************************************
 *                  HMI BOARD FUNCTIONS
********************************************************************************/
/*--- STATIC SETTING ----------------------------------------------------------*/
#define HI_IDENTIFICATION                   0x0020
#define HI_SERIAL_NUMBER                    0x0025
#define HI_TOOL_DESCRIPTION_1               0x0026
#define HI_TOOL_DESCRIPTION_2               0x0027
#define HI_DEVICE_ID_1                      0x0028
#define HI_DEVICE_ID_2                      0x0029

/*--- DYNAMIC SETTINGS --------------------------------------------------------*/
#define HI_TRIGGER_SETTINGS                 0x0001
#define HI_SPEED_SETTINGS                   0x0002
#define HI_BUTTON_SETTINGS                  0x0003
#define HI_SENSORS_SETTINGS                 0x0004
#define HI_CONFIG_SETTINGS                  0x0005
/*--- SPECIFIC REQUEST SETTINGS -----------------------------------------------*/
#define HI_REQUEST_BUTTONS_STATE            0x0004

/*--- COMMAND SETTINGS --------------------------------------------------------*/
#define POWER_OFF_TOOL                      0x55
#define CMD_MODE_FABTEST                    0x0002                              // Demande passage en mode TEST FABRICATION
#define RESET_TIME_JOURN                    0x0101                              // Demande reset compteur temps journalier
#define RESET_TIME_REV                      0x0102                              // Demande reset compteur temps revision
#define RESET_PMOY                          0x0103                              // Demande reset compteur puissance moyenne
#define RESET_ENERGIE                       0x0104                              // Demande reset compteur energie
#define ASK_CONSO                           0x0105                              // Demande consommmation machine
#define ASK_INFO                            0x0106                              // Demande infos machine
#define RESET_TIME_MOTOFF                   0x0107                              // Demande reset compteur temps moteur arr�t�
#define RESET_TIME_MOTON                    0x0108                              // Demande reset compteur temps moteur marche



/*******************************************************************************
 *                  BROADCAST BOARD FUNCTIONS
********************************************************************************/
/*--- STATIC SETTING ----------------------------------------------------------*/
/*--- DYNAMIC SETTINGS --------------------------------------------------------*/
/*--- SPECIFIC REQUEST SETTINGS -----------------------------------------------*/
/*--- COMMAND SETTINGS --------------------------------------------------------*/
#define RESET_VALUE                         0x01                                // Fonction demande reset registre
#define BC_MODE                             0x02                                // Changement mode: Normal, M�J, Test FAb
#define ASK_VALUE                           0x03                                // Fonction demande valeurs d'une fonction

#endif /* INC_FDCAN_DATAS_H_ */
