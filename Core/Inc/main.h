/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"
#include "veml6030.h"
#include "FDCAN_lib.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
extern FDCAN_HandleTypeDef hfdcan2;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TIMER_TICKS_PER_SECONDS 1000
#define RESET_Pin GPIO_PIN_2
#define RESET_GPIO_Port GPIOF
#define CAP_DROITE_Pin GPIO_PIN_0
#define CAP_DROITE_GPIO_Port GPIOA
#define CAP_DROITE_EXTI_IRQn EXTI0_1_IRQn
#define CAP_GAUCHE_Pin GPIO_PIN_1
#define CAP_GAUCHE_GPIO_Port GPIOA
#define CAP_GAUCHE_EXTI_IRQn EXTI0_1_IRQn
#define INTB_Pin GPIO_PIN_2
#define INTB_GPIO_Port GPIOA
#define INTB_EXTI_IRQn EXTI2_3_IRQn
#define SDB_Pin GPIO_PIN_3
#define SDB_GPIO_Port GPIOA
#define INT_LIGTH_Pin GPIO_PIN_4
#define INT_LIGTH_GPIO_Port GPIOA
#define BP_Pin GPIO_PIN_5
#define BP_GPIO_Port GPIOA
#define CAP_SECU_Pin GPIO_PIN_6
#define CAP_SECU_GPIO_Port GPIOA
#define LED_PCB_Pin GPIO_PIN_8
#define LED_PCB_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
