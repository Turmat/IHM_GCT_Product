/*
 * AS5510.h
 *
 *  Created on: 20 avr. 2023
 *      Author: turmat
 */

#ifndef INC_AS5510DRIVER_H_
#define INC_AS5510DRIVER_H_

#include "stm32g0b1xx.h"
#include "ErrorFile.h"

/******************************************************************************
 *                  GLOBAL CONSTANTS
******************************************************************************/
#define OWN_ADDRESS             0xA0

#define ADDR_AS5510             0x56
#define ADDR_LSB_AS5510         0x00
#define ADDR_MSB_AS5510         0x01
#define ADDR_SETTINGS_AS5510    0x02
#define ADDR_SENSIBILITE_AS5510 0x0B
#define DIRECT_POLARITY         0x00
#define INVERT_POLARITY         0x02
#define SENS_50mG               0x00
#define SENS_25mG               0x01
#define SENS_12mG               0x02
#define SENS_18mG               0x03

/******************************************************************************
 *                  PROTOYPES SECTION
******************************************************************************/

eERRORRESULT InitSensitivityAS5510(unsigned char Sensibilite);
eERRORRESULT InitPolarityAS5510(unsigned char Polarity);
eERRORRESULT InitModeAS5510(unsigned char SpeedMode);

eERRORRESULT AcquisitionAS5510(unsigned short *value);
eERRORRESULT ReadAS551O(unsigned char Address, uint16_t *value);

typedef struct AS5510Driver{
        unsigned char SensibiliteCapteur;
        unsigned char PreviousSensibiliteCapteur;
        unsigned char Polarity;
        unsigned char PreviousPolarity;
        uint16_t CapteurAddress;

}AS5510Driver;


#endif /* INC_AS5510DRIVER_H_ */
