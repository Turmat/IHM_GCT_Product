/*
 * 7segDisplay_driver.h
 *
 *  Created on: 20 avr. 2023
 *      Author: turmat
 */

#ifndef INC_7SEGDISPLAY_DRIVER_H_
#define INC_7SEGDISPLAY_DRIVER_H_


#include "IS31FL3736_Driver.h"

#define DIG1_WHITE   SW9
#define DIG1_RED     SW8
#define DIG2_WHITE   SW7
#define DIG2_RED     SW6
#define PICTO_WHITE  SW5
#define PICTO_RED    SW4
#define PICTO_GREEN  SW3
#define PICTO_BLUE   SW1

#define SEG7_NBR_1  (CS2 | CS3                              )
#define SEG7_NBR_2  (CS1 | CS2 | CS4 | CS5 | CS7            )
#define SEG7_NBR_3  (CS1 | CS2 | CS3 | CS4 | CS7            )
#define SEG7_NBR_4  (CS2 | CS3 | CS6 | CS7                  )
#define SEG7_NBR_5  (CS1 | CS3 | CS4 | CS6 | CS7            )
#define SEG7_NBR_6  (CS1 | CS3 | CS4 | CS5 | CS6 | CS7      )
#define SEG7_NBR_7  (CS1 | CS2 | CS3                        )
#define SEG7_NBR_8  (CS1 | CS2 | CS3 | CS4 | CS5 | CS6 | CS7)
#define SEG7_NBR_9  (CS1 | CS2 | CS3 | CS4 | CS5 | CS7      )
#define SEG7_NBR_0  (CS1 | CS2 | CS3 | CS4 | CS5 | CS6      )
#define SEG7_LETR_n (CS7 | CS5 | CS3                        )

//#define IS31_PAGE0 (uint8_t [2]) {0xFD, LED_Control_PAGE}, 2

#define PICTO_CRUISE_CTRL  PICTO_BLUE, CS5
#define PICTO_BOOST_CTRL  PICTO_BLUE, CS4
#define PICTO_BTRY_FULL PICTO_WHITE, (CS8 | CS2 | CS3 | CS4 )
#define PICTO_BTRY_75 PICTO_WHITE, (CS8 | CS2 | CS3 )
#define PICTO_BTRY_50 PICTO_WHITE, (CS8 | CS2  )
#define PICTO_BTRY_25 PICTO_RED, (CS8 )


#endif /* INC_7SEGDISPLAY_DRIVER_H_ */
