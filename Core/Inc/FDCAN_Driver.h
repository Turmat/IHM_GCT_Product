/*
 * FDCAN_Driver.h
 *
 *  Created on: 6 juin 2023
 *      Author: turmat
 */

#ifndef INC_FDCAN_DRIVER_H_
#define INC_FDCAN_DRIVER_H_



void can_callback(FDCAN_HandleTypeDef *fdcan, const FDCAN_RxHeaderTypeDef *header, const uint8_t *data);
eERRORRESULT CANStop (FDCAN_HandleTypeDef *hfdcan);
eERRORRESULT CANStart (FDCAN_HandleTypeDef *hfdcan);
CreateFDCAN_Filterlist ();
#endif /* INC_FDCAN_DRIVER_H_ */
