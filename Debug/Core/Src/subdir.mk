################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/AS5510Driver.c \
../Core/Src/ErrorHandler.c \
../Core/Src/FDCAN_Driver.c \
../Core/Src/FDCAN_lib.c \
../Core/Src/IS31FL3736_Driver.c \
../Core/Src/handleButton.c \
../Core/Src/main.c \
../Core/Src/stm32g0xx_hal_msp.c \
../Core/Src/stm32g0xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32g0xx.c \
../Core/Src/veml6030.c 

OBJS += \
./Core/Src/AS5510Driver.o \
./Core/Src/ErrorHandler.o \
./Core/Src/FDCAN_Driver.o \
./Core/Src/FDCAN_lib.o \
./Core/Src/IS31FL3736_Driver.o \
./Core/Src/handleButton.o \
./Core/Src/main.o \
./Core/Src/stm32g0xx_hal_msp.o \
./Core/Src/stm32g0xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32g0xx.o \
./Core/Src/veml6030.o 

C_DEPS += \
./Core/Src/AS5510Driver.d \
./Core/Src/ErrorHandler.d \
./Core/Src/FDCAN_Driver.d \
./Core/Src/FDCAN_lib.d \
./Core/Src/IS31FL3736_Driver.d \
./Core/Src/handleButton.d \
./Core/Src/main.d \
./Core/Src/stm32g0xx_hal_msp.d \
./Core/Src/stm32g0xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32g0xx.d \
./Core/Src/veml6030.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/%.o Core/Src/%.su Core/Src/%.cyclo: ../Core/Src/%.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32G0B1xx -c -I../Core/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc -I../Drivers/STM32G0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32G0xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Core-2f-Src

clean-Core-2f-Src:
	-$(RM) ./Core/Src/AS5510Driver.cyclo ./Core/Src/AS5510Driver.d ./Core/Src/AS5510Driver.o ./Core/Src/AS5510Driver.su ./Core/Src/ErrorHandler.cyclo ./Core/Src/ErrorHandler.d ./Core/Src/ErrorHandler.o ./Core/Src/ErrorHandler.su ./Core/Src/FDCAN_Driver.cyclo ./Core/Src/FDCAN_Driver.d ./Core/Src/FDCAN_Driver.o ./Core/Src/FDCAN_Driver.su ./Core/Src/FDCAN_lib.cyclo ./Core/Src/FDCAN_lib.d ./Core/Src/FDCAN_lib.o ./Core/Src/FDCAN_lib.su ./Core/Src/IS31FL3736_Driver.cyclo ./Core/Src/IS31FL3736_Driver.d ./Core/Src/IS31FL3736_Driver.o ./Core/Src/IS31FL3736_Driver.su ./Core/Src/handleButton.cyclo ./Core/Src/handleButton.d ./Core/Src/handleButton.o ./Core/Src/handleButton.su ./Core/Src/main.cyclo ./Core/Src/main.d ./Core/Src/main.o ./Core/Src/main.su ./Core/Src/stm32g0xx_hal_msp.cyclo ./Core/Src/stm32g0xx_hal_msp.d ./Core/Src/stm32g0xx_hal_msp.o ./Core/Src/stm32g0xx_hal_msp.su ./Core/Src/stm32g0xx_it.cyclo ./Core/Src/stm32g0xx_it.d ./Core/Src/stm32g0xx_it.o ./Core/Src/stm32g0xx_it.su ./Core/Src/syscalls.cyclo ./Core/Src/syscalls.d ./Core/Src/syscalls.o ./Core/Src/syscalls.su ./Core/Src/sysmem.cyclo ./Core/Src/sysmem.d ./Core/Src/sysmem.o ./Core/Src/sysmem.su ./Core/Src/system_stm32g0xx.cyclo ./Core/Src/system_stm32g0xx.d ./Core/Src/system_stm32g0xx.o ./Core/Src/system_stm32g0xx.su ./Core/Src/veml6030.cyclo ./Core/Src/veml6030.d ./Core/Src/veml6030.o ./Core/Src/veml6030.su

.PHONY: clean-Core-2f-Src

